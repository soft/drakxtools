package messages; # $Id: messages.pm 245997 2008-09-19 12:59:39Z pixel $




use common;

sub main_license {
    return cat_('/usr/share/doc/' . (($ENV{'LC_MESSAGES'} =~ m/ru_RU/) ? 'ru' : 'en') . '_license.html');
}

sub install_completed() {
#-PO: keep the double empty lines between sections, this is formatted a la LaTeX
N("Congratulations, installation is complete.
Remove the boot media and press Enter to reboot.


For information on fixes which are available for this release of Mandriva Linux,
consult the Errata available from:


%s


Information on configuring your system is available in the post
install chapter of the Official Mandriva Linux User's Guide.",
'http://www.mandriva.com/en/security/advisories');
}

1;
