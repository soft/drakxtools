#!/usr/bin/perl

use lib qw(/usr/lib/libDrakX);

# i18n: IMPORTANT: to get correct namespace (drakx-kbd-mouse-x11 and drak3d instead of libDrakX)
BEGIN { unshift @::textdomains, 'drakx-net', 'drakx-kbd-mouse-x11', 'drak3d' }

use standalone;
use common;
use interactive;
use any;
use authentication;
use network::network;
use security::level;
use background;
use autoinstall;
use check_min_sys_requirements;

use Geo::IP;

my $conf_file = '/etc/sysconfig/finish-install';
my %conf = getVarsFromSh($conf_file);
my $authentication = authentication::get();
my $security = security::level::get();
my $net = {};
my $locale;
my $timezone;
my $vnc_install;
my $vnc_postinstall;
my $new_user;
my $geoip_info;
my $bios_utc_time_diff;     # Difference (seconds) between UTC time and local BIOS time
network::network::read_net_conf($net);

# Reads hardware clock and returns it in form of seconds from Epoch
sub hwclock() {
    require Time::Local;
    require 'sys/ioctl.ph';

    # Open the RTC device and send it the RTC_RD_TIME ioctl
    my $f;
    my $data = "\0" x 36;                       # Output buffer is 9 integer values, 36 bytes
    open($f, '<', '/dev/rtc') or return undef;
    my $ret = ioctl($f, 0x80247009, $data);     # RTC_RD_TIME constant is 0x80247009
    close($f);
    return undef if (!defined($ret));           # ioctl returns undef for failure

    # Translate the output buffer into 9 Perl-variables
    my ($tm_sec, $tm_min, $tm_hour, $tm_mday, $tm_mon, $tm_year, $tm_wday, $tm_yday, $tm_isdst) = unpack('i9', substr($data, 0, 36));
    # And finally, convert into seconds (using timegm to avoid any timezone shifting - we need raw value here)
    return Time::Local::timegm($tm_sec, $tm_min, $tm_hour, $tm_mday, $tm_mon, $tm_year);
}

sub get_geoip_info() {
    # Obtain GeoIP data by our external IP (if available)
    my $ip = `wget --output-document=- --output-file=/tmp/myip-wget.log --server-response --timeout=3 --tries=1 http://www.rosalab.ru/myip`;
    $ip =~ s/[\r\n]//g;
    if ($ip) {
        # Obtain the GeoIP info
        my $gi = Geo::IP->open('/usr/share/GeoIP/GeoLiteCity.dat', GEOIP_STANDARD);
        if ($gi) {
            $geoip_info = $gi->record_by_addr($ip);
        }
        # Read the server timestamp (from HTTP response) to know the real UTC time.
        my $server_time;
        if (open(F, '<', '/tmp/myip-wget.log')) {
            require HTTP::Date;
            while (<F>) {
                if (m/^\s*Date:\s*(\S.*)/) {
                    $server_time = HTTP::Date::str2time($1);
                    last;
                }
            }
            close(F);
        }
        # Read the local hardware timer
        my $bios_time = hwclock();
        if (defined($server_time) and defined($bios_time)) {
            # Everything OK, save the difference. We might have lagged a couple of seconds,
            # or be out of sync from the server, but it doesn't matter: we'll use it for
            # choosing the timer mode (UTC/local) which is several hours difference.
            $bios_utc_time_diff = $bios_time - $server_time;
        }
    }
    unlink('/tmp/myip-wget.log');
}

$::isWizard = 1;
my $from_installer = ($ARGV[0] eq '--from-installer');       # Whether firstboot stage is launched directly from installer (chrooted)
my $from_live = (any::running_window_manager() ne 'drakx-matchbox-window-manager');     # Whether running from Live mode
my %conf_fb = getVarsFromSh('/etc/sysconfig/firstboot');
if ($conf_fb{FIRSTBOOT} eq 'yes') {
    $::Wizard_no_cancel = 1;
}
else {
    # When booting from ISO, get GeoIP data by our external IP (if available)
    get_geoip_info();
}

my $in = 'interactive'->vnew('su');
any::set_wm_hints_if_needed($in);
$in->{pop_wait_messages} = 0;

my $wait = $in->wait_message('', N("Please wait")) if ($from_installer && $from_live);

sub get_local_ips() {
	# Parsing ifconfig output by extracting IP addresses and eliminating loopbacks (127.*.*.*)
	return map { (m/inet (?:addr:)?\s*(\d+\.\d+\.\d+\.\d+)/ && !m/127\./) ? $1 : () } split(m/[\r\n]+/, `ifconfig`);
}

sub get_conf {
	my ($name) = @_;
	$conf{lc($name)} || $conf{uc($name)};
}

sub ask_sysreq() {
	return if (cat_('/proc/cmdline') =~ m/\binstall\b/);
	my $min_mem;
	my $total_ram;
	check_min_sys_requirements::min_system_requirements($min_mem);
	$total_ram = check_min_sys_requirements::total_mem_size();
	if ($total_ram < $min_mem) {
		my $warning;
		$warning .= '<span foreground="#d06000" weight="bold">' . N("System requirements warning\n") . '</span>';
		$warning .= N("Recommended system parameters:\n");
		$warning .= N("  RAM = ").$min_mem.N(" Mb\n");
		$warning .= N("\nYour system parameters:\n");
		$warning .= N("  RAM = ").$total_ram.N(" Mb\n");
		$warning .= N("\nYour system resources are too low for Live mode.\nHowever, you may continue at your own risk.");
		$in->ask_warn(N("System requirements warning"), $warning) or any::reboot();
	}
}

sub ask_license() {
    # Skip if autoinstallation is working
    return if ($autoinstall::enabled);
    local $::isWizard = 0;
    any::acceptLicense($in);
}

sub ask_vnc() {
	# If non-VNC installation then skip this step
	return if (!$vnc_install && !$vnc_postinstall);

	# Also skip if autoinstallation is working
	return if ($autoinstall::enabled);

	# Run VNC server
	system('x11vnc -display :0 -forever -o /var/log/x11vnc.log -bg');

	# Check for current IP address(es) and VNC server status
	my $status_info;
	my @ips = get_local_ips();
	if (`ps -A | grep x11vnc | grep -v grep` ne '') {
		$status_info = N("VNC server status:") . ' <span foreground="#00a000"><b>' . N("Running") . "</b></span>\n" .
		               N("You can continue installation remotely.") . "\n\n" .
		               ((scalar(@ips) > 1) ? N("This computer's IP addresses are:") : N("This computer's IP address is:")) . ' <b>' . join(', ', @ips) . '</b>';
	}
	else {
		$status_info = N("VNC server status:") . ' <span foreground="#d00000"><b>' . N("Failed") . "</b></span>\n" .
		               N("Please, continue installation locally.");
	}

	# Display collected information
	$in->ask_from_({
		title => N("VNC Status")
	}, [
		{
			label => $status_info,
			type => 'label'
		},
		{},
		{
			val => \(my $tmpstr = N("x11vnc output")),
			type => 'button',
			install_button => 1,
			do_not_expand => 1,
			clicked => sub { any::display_text('x11vnc', '' . cat_('/var/log/x11vnc.log')); }
		},
		{
			val => \(my $tmpstr = N("ifconfig output")),
			type => 'button',
			install_button => 1,
			do_not_expand => 1,
			clicked => sub { any::display_text('ifconfig', $ifconfig); }
		}
	]);
}

sub ask_language() {
    require lang;
    $locale = lang::read();
    my ($lang) = cat_("/proc/cmdline") =~ /\blang=(.+?)\b/;
    my $h = lang::lang_to_ourlocale($lang);
    if ($lang && member($h->{lang}, lang::list_langs(exclude_non_installed => 1))) {
	put_in_hash($locale, $h);
	lang::set($locale);
    }

    my $locale_geoip;
    if ($geoip_info) {
	$locale_geoip = lang::lang_to_ourlocale(lang::c2locale($geoip_info->country_code));
    }

    if ($autoinstall::enabled) {
	$locale->{utf8} = $autoinstall::params{'UTF8'};
	if ($autoinstall::params{'LANGUAGE_USE_GEOIP'} && $locale_geoip) {
	    $locale = $locale_geoip;
	}
	else {
	    $locale->{lang} = $autoinstall::params{'LANGUAGE'};
	}
    }
    else {
	if ($locale_geoip) {
	    $locale = $locale_geoip;
	}
    }
    any::selectLanguage_standalone($in, $locale, $autoinstall::enabled);
    lang::write_and_install($locale, $in->do_pkgs);
}

sub ask_keyboard() {
    require keyboard;
    my $keyboard = $locale ? keyboard::lang2keyboard($locale->{lang}) : keyboard::read_or_default();

    if ($autoinstall::enabled) {
	$keyboard->{KEYBOARD} = $autoinstall::params{'KEYBOARD'};
	$keyboard->{GRP_TOGGLE} = $autoinstall::params{'KEYBOARD_TOGGLE'};
    }
    else {
      choose:
	$keyboard->{KEYBOARD} = $in->ask_from_listf(N("Keyboard"),
                                                    N("Please, choose your keyboard layout."),
                                                    sub { translate(keyboard::KEYBOARD2text($_[0])) },
                                                    [ keyboard::KEYBOARDs() ],
                                                    $keyboard->{KEYBOARD}) or return;

	keyboard::group_toggle_choose($in, $keyboard) or goto choose;
    }

    keyboard::configure_and_set_standalone($keyboard);
}

#- TIMEZONE=simplified: do not ask timezone and make ntp settings advanced
sub ask_timezone() {
    require timezone;

    my $timezone_geoip;
    if (!$geoip_info) {
	# Initial external IP retrieving often fails, let's try again
	get_geoip_info();
    }
    if ($geoip_info) {
	$timezone_geoip = ($geoip_info->time_zone or timezone::bestTimezone($geoip_info->country_code));
    }

    if ($autoinstall::enabled) {
	if ($autoinstall::params{'TIMEZONE_USE_GEOIP'} && $timezone_geoip) {
	    $timezone = { timezone => $timezone_geoip };
	}
	else {
	    $timezone = { timezone => $autoinstall::params{'TIMEZONE'} };
	}
	$timezone->{UTC} = $autoinstall::params{'UTC'};
	$timezone->{ntp} = ($autoinstall::params{'NTP'} ? $autoinstall::params{'NTP_SERVER'} : '');
    }
    else {
	$timezone = timezone::read();
	# First try using GeoIP for setting timezone
	if ($timezone_geoip) {
	    # Found timezone from GeoIP (either directly, or default for the current country), using it
	    $timezone->{timezone} = $timezone_geoip;
	}
	elsif ($locale->{country}) {
	    # If GeoIP did not help (or was not used), fall back to default "best choice" approach
	    $timezone->{timezone} = timezone::bestTimezone($locale->{country});
	}
	any::configure_timezone($in, $timezone, 'ask_gmt', lc(get_conf('TIMEZONE')) eq 'simplified', $bios_utc_time_diff);
    }

    $in->do_pkgs->ensure_is_installed('ntp') if $timezone->{ntp};
    timezone::write($timezone);

    #- reload sys clock from hc once we know the real timezone
    timezone::reload_sys_clock($timezone);
}

#- COUNTRY=simplified: guess the country from timezone, do not ask
sub ask_country() {
    require lang;
    $locale ||= lang::read();
    require Time::ZoneInfo;
    my $zones = Time::ZoneInfo->new;
    if ($timezone && $zones) {
        #- guess only if timezone has been asked already
        if (my $zone = $zones->current_zone) {
            if (my $country_code = $zones->country($zone)) {
                $locale->{country} = $country_code;
            }
        }
    }
    any::selectCountry($in, $locale) if lc(get_conf('COUNTRY')) ne 'simplified';
    lang::write_and_install($locale, $in->do_pkgs);
}

sub ask_network() {
    require network::tools;
    return if network::tools::has_network_connection();

    #- test again connection after waiting for network-up service
    my $w = $in->wait_message(N("Please wait"), N("Testing your connection..."));
    services::start('network-up');
    undef $w;
    return if network::tools::has_network_connection();

    require network::netconnect;
    my $modules_conf = modules::any_conf->read;
    network::netconnect::real_main($net, $in, $modules_conf);
    $modules_conf->write;
}

sub ask_urpmi() {
    #- configure urpmi media if no media are configured
    run_program::get_stdout('urpmq', '--list-media') and return;
    any::urpmi_add_all_media($in);
}

sub set_authentication {
    my ($superuser) = @_;
    authentication::set_root_passwd($superuser, $authentication);
    my $ok = eval {
	authentication::set($in, $net, $authentication) or return;
	network::network::write_network_conf($net);
        1;
    };
    $in->ask_warn(N("Error"), formatError($@)) if $@;
    return $ok;
}

sub ask_authentication() {
    my $meta_class = { getVarsFromSh("/etc/sysconfig/system") }->{META_CLASS};
    my $superuser = {};
    authentication::ask_root_password_and_authentication($in, $net, $superuser, $authentication, $meta_class, $security);
    set_authentication($superuser) or goto &ask_authentication;
}

# Escapes a string so that it could be used as a command-line parameter
# Particularly: the string is enclosed in single quotes, and any single quote inside it is replaced with '\''
# (the opening quote is closed, then an escaped single-quote is appended and new quote is opened)
sub escape_cmdline_param($) {
	my ($s) = @_;
	$s =~ s/\'/'\\\''/g;
	return "'$s'";
}

#- USERS=with_root: asks both root and user accounts
#- USER_RENAME_FROM=<old user>: create the new user by renaming <old user>
#- USER_AUTOLOGIN_FIRST: configure autologin for the first added user
sub ask_users() {
    my %desktop = getVarsFromSh("$::prefix/etc/sysconfig/desktop");
    if ($desktop{DESKTOP} eq 'GNOME') {
        # drop live system user
        system('userdel -r live');
    }

    my $auth;      # Local (0) or MSAD (1)
    my $superuser;
    if ($autoinstall::enabled) {
	$auth = ($autoinstall::params{'AUTH_TYPE'} eq 'msad');
	$superuser = {
	    'disabled'  => $autoinstall::params{'LOCAL_ROOT_DISABLED'},
	    'password'  => $autoinstall::params{'LOCAL_ROOT_PASSWORD'},
	    'password2' => $autoinstall::params{'LOCAL_ROOT_PASSWORD'}
	};
    }
    else {
	$auth = 0;      # Local users by default
	$superuser = {
	    'disabled'  => 1,
	    'password'  => '',
	    'password2' => ''
	};
    }

    # Cycle to allow retrying to join domain in case invalid parameters were specified the first time
    my $completed = 0;
    do {
        if (!$autoinstall::enabled && $in->do_pkgs->is_installed('rpbis')) {
            # Allow to select from local and MSAD authentication
            my $r = $in->ask_from_(
                {
                    title => N("Authentication"),
                    messages => [ N("Please, choose authentication method:") ]
                },
                [
                    {
                        val => \$auth,
                        type => 'list',
                        list => [ 0, 1 ],
                        format => sub { ( N("Local users"), N("Domain users") )[$_[0]] }
                    }
                ]
            );
        }
        if ($auth == 0) {
            # Creating local user

            my $users = [];

            if ($autoinstall::enabled) {
                # Set up the local root
                if ($superuser->{'disabled'}) {
                    # Disable root login
                    system('passwd -l root');
                }
                else {
                    # Set the specified root password
                    set_authentication($superuser);
                }

                # Set up the local user
                $users = [
                    {
                        'icon'      => $autoinstall::params{'USER_ICON'},
                        'realname'  => $autoinstall::params{'USER_REALNAME'},
                        'name'      => $autoinstall::params{'USER_NAME'},
                        'password'  => $autoinstall::params{'USER_PASSWORD'},
                        'password2' => $autoinstall::params{'USER_PASSWORD'},
                        'shell'     => $autoinstall::params{'USER_SHELL'},
                        'uid'       => $autoinstall::params{'USER_UID'},
                        'gid'       => $autoinstall::params{'USER_GID'},
                        'groups'    => [ split(m/[,;\s]+/, $autoinstall::params{'USER_GROUPS'}) ]
                    }
                ];
                any::add_users($users, $authentication);
            }
            else {
                $superuser = to_bool(lc(get_conf('USERS')) eq 'with_root') && {};
                any::ask_user_and_root($in, $superuser, $users, $security);
                push @{$users->[0]{groups}}, 'wheel', 'sambashare', 'users';
                my $old_user = get_conf('USER_RENAME_FROM');
                if (@$users && $old_user) {
                    $users->[0]{rename_from} = $old_user;
                    $users->[0]{home} ||= '/home/' . $users->[0]{name};
                }
                my $autologin = any::get_autologin();
                my $autologin_first = ($autologin->{user} eq $old_user || lc(get_conf('USER_AUTOLOGIN_FIRST')) eq "yes") && $autologin->{desktop};
                if ($superuser) {
                    set_authentication($superuser) or goto &ask_users;
                }
                any::add_users($users, $authentication);
                if ($autologin_first) {
                    $autologin->{user} = $users->[0]{name};
                    $autologin->{desktop} = $autologin_first;
                    any::set_autologin($in->do_pkgs, $autologin);
                }
                my $finit_conf = "/etc/finit.conf";
                substInFile {
                    s/^user .*//;
                    $_ .= "user $users->[0]{name}\n" if eof;
                } $finit_conf if -e $finit_conf;
                if ($old_user) {
                    #- replace home path in user config files
                    my $old_home = "/home/$old_user";
                    my $new_home = "/home/$users->[0]{name}";
                    run_program::run(qq(grep -rl $old_home $new_home/.??* | while read f; do perl -pi -e 's,$old_home,$new_home,g' "\$f"; done));
                    #- give console rights for current session
                    my $console_dir = "/var/run/console";
                    cp_f($console_dir . "/" . $old_user, $console_dir . "/" . $users->[0]{name}) if -e $console_dir . "/" . $old_user;
                }
            }
            $new_user = $users->[0]{name};
            # Local user is always successful
            $completed = 1;
        }
        else {
            # Joining MS AD via PBIS
            my $_wait;
            my $domain_params;
            if ($autoinstall::enabled) {
                $domain_params = {
                    'hostname'   => build_hostname(),
                    'domain'     => $autoinstall::params{'MSAD_DOMAIN'},
                    'ou_default' => ($autoinstall::params{'MSAD_OU'} eq ''),
                    'ou'         => $autoinstall::params{'MSAD_OU'},
                    'admin_name' => $autoinstall::params{'MSAD_ADMIN_NAME'},
                    'admin_pass' => $autoinstall::params{'MSAD_ADMIN_PASSWORD'}
                };
            }
            else {
                # Requesting all the data needed for joining domain
                $domain_params = {
                    'hostname'   => '',
                    'domain'     => '',
                    'ou_default' => 1,
                    'ou'         => '',
                    'admin_name' => 'Administrator',
                    'admin_pass' => ''
                };
                my $validate_nonempty = sub {
                    # Auxiliary function that validates an editbox to be non-empty and shows the error message otherwise
                    my ($value, $error_msg) = @_;
                    if ($value =~ m/^\s*$/) {
                        $in->ask_warn('', $error_msg);
                        return;
                    }
                    else {
                        return 'ok';
                    }
                };
                $in->ask_from_(
                    {
                        title => N("Join domain"),
                        focus_first => 1,
                        advanced_label => N("Local root account"),
                        advanced_messages => [ '', N("Local root account:") ]
                    },
                    [
                        {
                            label => N("Domain parameters"),
                            title => 1
                        },
                        {
                            label => N("Hostname:"),
                            val => \$domain_params->{'hostname'},
                            validate => sub { $validate_nonempty->($domain_params->{'hostname'}, N("Hostname must not be empty!")); }
                        },
                        {
                            label => N("Domain name:"),
                            val => \$domain_params->{'domain'},
                            validate => sub { $validate_nonempty->($domain_params->{'hostname'}, N("Domain name must not be empty!")); }
                        },
                        {
                            label => N("Default OU"),
                            val => \$domain_params->{'ou_default'},
                            type => 'bool'
                        },
                        {
                            label => N("Custom OU:"),
                            val => \$domain_params->{'ou'},
                            disabled => sub { $domain_params->{'ou_default'} }
                        },
                        {
                            label => N("Domain administrator account"),
                            title => 1
                        },
                        {
                            label => N("User name:"),
                            val => \$domain_params->{'admin_name'},
                            validate => sub { $validate_nonempty->($domain_params->{'hostname'}, N("Administrator name must not be empty!")); }
                        },
                        {
                            label => N("Password:"),
                            val => \$domain_params->{'admin_pass'},
                            hidden => 1
                        },
                        {
                            advanced => 1
                        },
                        {
                            label => N("Disable account"),
                            val => \$superuser->{'disabled'},
                            type => 'bool',
                            advanced => 1
                        },
                        {
                            label => N("Password:"),
                            val => \$superuser->{'password'},
                            hidden => 1,
                            advanced => 1,
                            disabled => sub { $superuser->{'disabled'} },
                            validate => sub { authentication::check_given_password($in, $superuser, 0); }
                        },
                        {
                            label => N("Password (again):"),
                            val => \$superuser->{'password2'},
                            hidden => 1,
                            advanced => 1,
                            disabled => sub { $superuser->{'disabled'} }
                        }
                    ]
                );
                $_wait = $in->wait_message(N("Authentication"), N("Please wait"));
            }
            {
                # Make sure hostname is only saved but not applied to the current system if we are running from installer
                local $::isInstall = ($from_installer ? 1 : $::isInstall);
                network::network::write_hostname($domain_params->{'hostname'});
            }
            # Hostname already specified, skip the separate step for it
            $conf{'hostname'} = 'no';
            my $res = system('domainjoin-cli join ' . ($domain_params->{'ou_default'} ? '' : '--ou ' . escape_cmdline_param($domain_params->{'ou'}) . ' ') . escape_cmdline_param($domain_params->{'domain'}) . ' ' . escape_cmdline_param($domain_params->{'admin_name'}) . ' ' . escape_cmdline_param($domain_params->{'admin_pass'}) . ' >/tmp/domainjoin.log 2>&1');
            undef $_wait;
            if ($res != 0) {
                # Something went wrong...
                if ($autoinstall::enabled) {
                    # In autoinstallation mode log the error and skip to the next step
                    log::l('Failed to join domain ' . $domain_params->{'domain'});
                }
                else {
                    # In normal mode ask user to retry the action. If no, skipping to the next step.
                    $completed = !any::ask_yesorno_showlog($in, N("Error"), N("Failed to join the specified domain. Try again with different parameters?"), '/tmp/domainjoin.log', N("Output log"), N("domainjoin-cli output"));
                    unlink('/tmp/domainjoin.log');
                    next;
                }
            }
            else {
                # Joined successfully, exit the cycle
                $completed = 1;
                # Set up the local root
                if ($superuser->{'disabled'}) {
                    # Disable root login
                    system('passwd -l root');
                }
                else {
                    # Set the specified root password
                    authentication::set_root_passwd($superuser, $authentication);
                }
            }
        }
    } until ($completed || $autoinstall::enabled)
}

sub build_hostname() {
	my $template;

	# Construct the replacement list to construct hostname from template
	my %replacements = ();
	# %u is the user name
	# %d is the domain name
	if ($autoinstall::enabled) {
		$template = $autoinstall::params{'HOSTNAME'};
		$replacements{'%u'} = $autoinstall::params{'USER_NAME'};
		$replacements{'%d'} = $autoinstall::params{'MSAD_DOMAIN'};
	}
	else {
		$template = '%u-%m';
		$replacements{'%u'} = $new_user;
	}
	# %m is the machine name
	$replacements{'%m'} = guess_machine_name();
	if (!$replacements{'%m'}) {
		$replacements{'%m'} = (guess_laptop() ? 'laptop' : 'desktop');
	}
	# %r is the randomly generated suffix (to have different hostnames for identical machines)
	$replacements{'%r'} = 'X' x 8;
	$replacements{'%r'} =~ s/./('a'..'z', '0'..'9')[int(rand(35))]/ge;

	# Perform the replacement
	foreach my $pattern (keys(%replacements)) {
		$template =~ s/$pattern/$replacements{$pattern}/g;
	}
	# Removing leading and trailing dots and dashes if any
	$template =~ s/^[.\-]+//;
	$template =~ s/[.\-]+$//;

	return $template;
}

# Tries to guess the computer model name (undef if no meaningful name found)
sub guess_machine_name() {
	my $res = `dmidecode --string system-manufacturer`;
	if (!$res) {
		return undef;
	}
	if (($res =~ m/to[\s\-]+be[\s\-]+filled/i) || ($res =~ m/system[\s\-]+manufacturer/i)) {
		return undef;
	}

	if ($res =~ m/bochs|vmware/i) {
		# Skip VirtualBox since it sets an appropriate system-product-name.
		$res = 'Virtual-Machine';
	}
	else {
		if ($res =~ m/apple/i) {
			# MacBook4,1 - strip the 4,1
			$res =~ s/[^a-z\s]//ig;
		}
		else {
			my $key = (($res =~ m/lenovo|ibm/i) ? 'system-version' : 'system-product-name');
			$res = `dmidecode --string $key`;
			if (!$res) {
				return undef;
			}
			# In some cases dmidecode returns text with comments - strip them and join the rest of lines together
			$res = join(" ", grep(!m/^#/, split(m/[\x0d\x0a]+/, $res)));
		}
	}

	# Replace all invalid characters with dashes and remove resultant statring and trailing dashes
	$res =~ s/[^a-z0-9]+/-/ig;
	$res =~ s/^-+//;
	$res =~ s/-+$//;
	if ($res =~ m/not-available/i) {
		return undef;
	}
	return $res;
}

# Tries to guess whether the machine is a laptop (1) or desktop (0)
sub guess_laptop() {
	# Based on laptop-detect tool

	# 1. Check Mac batteries
	if (-d '/proc/pmu') {
		my @batteries = grep(m/Battery/, cat_('/proc/pmu/info'));
		foreach (@batteries) {
			if ((m/^[^:]*:\s*(\S+)/) && ($1 != 0)) {
				print "We're a laptop (Mac: batteries found)\n";
				return 1;
			}
		}
		return 0;
	}

	# 2. Try DMI information: Chassis type
	my $dmitype = `dmidecode --string chassis-type`;
	$dmitype =~ s/[\r\n]+//g;
	if (($dmitype eq 'Notebook') || ($dmitype eq 'Portable')) {
		print "We're a laptop (dmidecode returned $dmitype)\n";
		return 1;
	}

	# 3. Check for any ACPI batteries
	system('modprobe', 'battery');
	if (-d '/sys/class/power_supply') {
		if (system('grep -q Battery /sys/class/power_supply/*/type') == 0) {
			print "We're a laptop (ACPI batteries found)\n";
			return 1;
		}
	}
	# Old interface:
	if (-d '/proc/acpi/battery') {
		my $results = `find /proc/acpi/battery -mindepth 1 -type d`;
		$results =~ s/[\r\n]+//g;
		if ($results ne '') {
			print "We're a laptop (ACPI batteries found)\n";
			return 1;
		}
	}


	# 4. Check for APM batteries. This sucks, because we'll only get a valid response
	# if the laptop has a battery fitted at the time
	if (-f '/proc/apm') {
		my $battery = `awk '{print $6}' </proc/apm`;
		$battery =~ s/[\r\n]+//g;
		if (($battery ne '0xff') && ($battery ne '0x80')) {
			print "We're a laptop (APM batteries found)\n";
			return 1;
		}
	}

	print "We're not on a laptop (no relevant hint found)\n";
	return 0;
}

sub ask_hostname() {
    if ($autoinstall::enabled || !$net->{network}{HOSTNAME} || ($net->{network}{HOSTNAME} eq 'localhost.localdomain')) {
	$net->{network}{HOSTNAME} = build_hostname();
    }
    if (!$autoinstall::enabled) {
	$in->ask_from_({ messages => N("Please, specify your host name:"),
	                 title => N("Hostname"), focus_first => 1 },
	               [ { val => \$net->{network}{HOSTNAME}, type => 'entry' } ]
	);
    }
    {
        # Make sure hostname is only saved but not applied to the current system if we are running from installer
        local $::isInstall = ($from_installer ? 1 : $::isInstall);
        network::network::write_hostname($net->{network}{HOSTNAME});
    }
}

sub ask_services() {
	# List of services
	my $services = {
		'smb'        => { STATE => 'auto', NAME => "Samba",    DESCRIPTION => N("Sharing folders and printers") },
#		'mdmonitor'  => { STATE => 'auto', NAME => "Linux MD", DESCRIPTION => N("Software RAID support") },
#		'openvpn'    => { STATE => 'auto', NAME => "OpenVPN",  DESCRIPTION => N("Virtual Private Network") },
		'sshd'       => { STATE => 'auto', NAME => "sshd",     DESCRIPTION => N("OpenSSH server") },
		'cups'       => { STATE => 'auto', NAME => "CUPS",     DESCRIPTION => N("Print server") },
#		'nfs-server' => { STATE => 'auto', NAME => "NFS",      DESCRIPTION => N("Network File System server") },
#		'pppoe'      => { STATE => 'auto', NAME => "PPPoE",    DESCRIPTION => N("Point-to-Point Protocol over Ethernet") },
#		'pptp'       => { STATE => 'auto', NAME => "PPTP",     DESCRIPTION => N("Point-to-Point Tunneling Protocol") },
	};
	my $checkboxes = [];
	foreach my $svc (sort(keys(%$services))) {
		if ($autoinstall::enabled) {
			# Get the desired service states from the config
			my $key_name = uc($svc);
			$key_name =~ s/-/_/g;
			my $state = $autoinstall::params{'SERVICE_' . $key_name};
			if (defined($state)) {
				$services->{$svc}{STATE} = $state;
			}
		}
		if ($services->{$svc}{STATE} eq 'auto') {
			# For 'auto' values get the current state of the service
			$services->{$svc}{STATE} = (system("systemctl is-enabled $svc.service") ? 0 : 1);
		}

		# Form the checkbox control for the dialog
		push @$checkboxes, {
			val => \$services->{$svc}{STATE},
			type => 'bool',
			text => $services->{$svc}{NAME} . ' (' . $services->{$svc}{DESCRIPTION} . ')',
			alignment => 'left'
		};
	}

	if (!$autoinstall::enabled) {
		# Display the dialog so that user could select the services
		$in->ask_from_({ messages => N("Please, specify which services should run at start-up:"),
				 title => N("Services") },
				 $checkboxes
		);
	}

	# Perform enabling/disabling services according to what user specified
	my @failed_enable = ();
	my @failed_disable = ();
	foreach my $svc (keys(%$services)) {
		my @sysctl_cmds;     # List of systemctl commands to execute
		my $failed_list;     # Link to list of failures for enabling/disabing (separately)
		if ($services->{$svc}{STATE}) {
			# For enabling the service, call 'systemclt enable'
			@sysctl_cmds = ('enable');
			# If we are booting the newly installed system, also start the service immediately
			push(@sysctl_cmds, 'start') if (!$from_installer);
			$failed_list = \@failed_enable;
		}
		else {
			# For disabling the service, call 'systemclt enable'
			@sysctl_cmds = ('disable');
			# If we are booting the newly installed system, stop the service before disabling it
			unshift(@sysctl_cmds, 'stop') if (!$from_installer);
			$failed_list = \@failed_disable;
		}

		# Try to call systemctl with necessary commands for the service
		foreach my $cmd (@sysctl_cmds) {
			if (system("systemctl $cmd $svc.service") != 0) {
				# If one of the commands fails, remember the failed service
				# and don't run any more commands on it
				push(@$failed_list, $svc);
				last;
			}
		}
	}

	# Collect information about failures if any
	my $warn_msg = '';
	if (scalar(@failed_enable) > 0) {
		$warn_msg .= N("Failed to enable the following services:\n") . join(', ', map { $services->{$_}{NAME} } @failed_enable) . "\n\n";
	}
	if (scalar(@failed_disable) > 0) {
		$warn_msg .= N("Failed to disable the following services:\n") . join(', ', map { $services->{$_}{NAME} } @failed_disable) . "\n\n";
	}

	# Display message about failures
	if ($warn_msg) {
		$in->ask_warn(N("Services"), $warn_msg);
	}
}

sub ask_encrypt_home() {
    my $user = { name => get_conf('ENCRYPT_HOME_USER'), device => get_conf('ENCRYPT_HOME_DEVICE') };
    any { !defined $_ } values %$user and return;
    $in->ask_from(N("Encrypted home partition"), N("Please enter a password for the %s user", $user->{name}),
                  [
                      { label => N("Password"), val => \$user->{password},  hidden => 1 },
                      { label => N("Password (again)"), val => \$user->{password2}, hidden => 1 },
                  ],
                  complete => sub {
                      authentication::check_given_password($in, $user, 6) or return 1,0;
                      return 0;
                  });
    authentication::write_passwd_user($user, $authentication);
    encrypt_home($user);
}

sub encrypt_home {
    my ($user) = @_;
    my $device = $user->{device};
    my $mapper = '/dev/mapper/' . $user->{name};
    my $home = '/home/' . $user->{name};
    my $wait = $in->wait_message(N("Encrypted home partition"), N("Creating encrypted home partition"));

    substInFile {
        s/^volume $user->{name}.*//;
        $_ .= "volume $user->{name} crypt - $device $home - - -\n" if eof;
    } $::prefix . '/etc/security/pam_mount.conf';
    authentication::set_pam_authentication('mount');

    run_program::raw({ root => $::prefix, sensitive_arguments => 1 },
                     "echo -e $user->{password} | cryptsetup luksFormat $device");
    run_program::raw({ root => $::prefix, sensitive_arguments => 1 },
                     "echo -e $user->{password} | cryptsetup luksOpen $device $user->{name}");
    $wait = $in->wait_message(N("Encrypted home partition"), N("Formatting encrypted home partition"));
    run_program::rooted($::prefix, 'mke2fs', '-qj', '-m', 0, '-L', 'Home', $mapper);

    my $old_home;
    if (-d $::prefix . $home) {
        #- if already existing, move home to a temporary folder
        require File::Temp;
        $old_home = File::Temp::tempdir(DIR => dirname($::prefix . $home));
        rmdir $old_home;
        rename $::prefix . $home, $old_home;
    }

    mkdir_p($::prefix . $home);
    run_program::rooted($::prefix, 'mount', $mapper, $home);

    if ($old_home) {
        #- copy previous home back
        require File::Copy::Recursive;
        File::Copy::Recursive::dirmove($old_home, $::prefix . $home);
    }
    run_program::rooted($::prefix, 'chown', '-R', join(':', ($user->{name}) x 2), $home);
    rmdir $::prefix . $home . '/lost+found';

    run_program::rooted($::prefix, 'umount', $home);
    run_program::rooted($::prefix, 'cryptsetup', 'luksClose', $user->{name});
    undef $wait;
}

sub ask_remove_unused_packages {
    require pkgs;
    local $::prefix;
    pkgs::remove_unused_packages($in, $in->do_pkgs, '/');
}

sub call {
    my ($step_name) = @_;
    my $f_name = 'ask_' . $step_name;
    if (lc(get_conf($step_name)) eq 'no') {
        log::l("ignoring $f_name");
    } else {
        log::l("calling $f_name");
        my $f = $::{$f_name} or internal_error "bad function $f_name";
        eval { $f->() };
        if ($@ =~ m/^wizcancel\b/) {
            any::reboot();
        }
        log::l("$f_name failed: $@") if $@;
    }
}

# Prints to STDOUT immediately (without buffering)
sub print_flushed {
    my $old_handle = select(STDOUT);
    my $old_flush = $|;
    $| = 1;
    print @_;
    $| = $old_flush;
    select($old_handle);
}

background::show_bg_window if (!$from_installer);

$vnc_install = (cat_('/proc/cmdline') =~ m/\bvncinstall\b/);
$vnc_postinstall = (get_conf('vncserver') eq 'yes');

if ($from_installer) {
    # Indicate that GUI is about to appear, so that calling draklive-install knew when to hide its window
    print_flushed("GUI start.\n");
}

call('vnc');
call('language');
call('sysreq');
call('license');
# "Previous" button isn't functiunnal and acts like "next" (#25349)
$::Wizard_no_previous = 1;
call('keyboard');
call('timezone');
call('country');
call('network');
if (defined $::WizardWindow) {
    $::WizardWindow->destroy;
    undef $::WizardWindow;
}
# Duct tape for setting the window title in Live mode: it remains unchanged for all wizard pages,
# so the first displayed dialog sets it to "User management", and it remains unchanged for all further steps.
# By showing the wait dialog (which disappears almost instantly) we set the title we want.
if ($from_installer && $from_live) {
    $in->wait_message(N("Initial setup"), N("Please wait"));
    any::center_window($::WizardWindow);
}
call('remove_unused_packages');
call('urpmi');
$::Wizard_pix_up = 'redhat-config-users';
call('authentication');
call('users');
call('hostname');
call('services');
call('encrypt_home');

if ($from_installer) {
    # Indicate that GUI is finished, so that calling draklive-install knew when to re-display its window
    print_flushed("GUI end.\n");
}

setVarsInSh($conf_file, { FINISH_INSTALL => 'no' });
if ($vnc_install) {
	MDK::Common::System::addVarsInSh('/etc/draklive-install.d/sysconfig/finish-install', {vncserver => 'yes'});
}
if ($vnc_postinstall) {
	system('killall x11vnc');
}

background::hide_bg_window if (!$from_installer);

if ($autoinstall::enabled && ($conf_fb{FIRSTBOOT} eq 'yes')) {
	# Installation finished, remove the autoinstallation config so that accidental launches of installer did not destroy the system
	rm_rf('/etc/sysconfig/autoinstall');
}

$in->exit(0);
