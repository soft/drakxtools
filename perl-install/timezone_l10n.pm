################################################################################
# This is a fake package for keeping timezone name strings for translating.
# Timezone names are taken from the file system and are not present in the code,
# so generating pot files with perl_checker would remove all the country/city
# names. This file contains all the strings in plain text to keep them in
# localization files.
#
# When translating, please, remove the "TZ:" prefix from the translation. It's
# used for differentiating timezone names from identical abbreviations used in
# other places (e.g. GB is used for GigaBytes, but here it's Great Britain).
#
# The list of strings can be generated semi-automatically. To update this file
# with the names taken from your current system, run the commands:
# $ cd /usr/lib/libDrakX
# $ sudo perl -e 'use timezone_l10n; timezone_l10n::update_timezone_l10n();'

package timezone_l10n;

use strict;
use warnings;

use lib '/usr/lib/libDrakX';
use common;
use timezone;

# Modifies this file itself, updating the list of timezones.
# Input arguments:
#    $tz_filename - [optional] path to file with list of timezones
#                   (if omitted, contents of /usr/share/zoneinfo is taken)
# Return value:
#    none
sub update_timezone_l10n(;$) {
	# Optional argument
	my ($tz_filename) = @_;

	# First, try to open our file as input and a new file as output
	my $o;
	my $o_filename = 'timezone_l10n.pm.new';
	my $i;
	my $i_filename = 'timezone_l10n.pm';
	open($o, '>', $o_filename) or die "Failed to open $o_filename for writing: $!";
	open($i, '<', $i_filename) or die "Failed to open $i_filename for reading: $!";

	# Next, copy all the lines before the auto-generated part to the new file
	while (<$i>) {
		print $o $_;
		# Stop if reached the delimiter
		last if (m/^# The following part will be replaced/);
	}
	close($i);
	print $o "\n";

	# Get the list of timezones and print it into our file, prefixing with "TZ:" and
	# wrapping with N_()
	my @tz;
	if ($tz_filename) {
		open($i, '<', $tz_filename) or die "Failed to open $tz_filename for reading: $!";
		# From each line remove the EOL characters and skip the empty lines
		@tz = map { s/[\r\n]//; $_ || (); } <$i>;
		close($i);
	}
	else {
		@tz = timezone::getTimeZones();
	}
	foreach (@tz) {
		print $o "N_(\"TZ:$_\");\n";
	}
	print $o "\n1;\n";
	close($o);

	# Now that everything went fine, replace the old file with the new one
	unlink($i_filename) or die "Failed to delete the old $i_filename: $!";
	rename($o_filename, $i_filename) or die "Failed to rename $o_filename to $i_filename: $!";
}

################################################################################
# The following part will be replaced on updating

N_("TZ:Africa/Abidjan");
N_("TZ:Africa/Accra");
N_("TZ:Africa/Addis_Ababa");
N_("TZ:Africa/Algiers");
N_("TZ:Africa/Asmara");
N_("TZ:Africa/Asmera");
N_("TZ:Africa/Bamako");
N_("TZ:Africa/Bangui");
N_("TZ:Africa/Banjul");
N_("TZ:Africa/Bissau");
N_("TZ:Africa/Blantyre");
N_("TZ:Africa/Brazzaville");
N_("TZ:Africa/Bujumbura");
N_("TZ:Africa/Cairo");
N_("TZ:Africa/Casablanca");
N_("TZ:Africa/Ceuta");
N_("TZ:Africa/Conakry");
N_("TZ:Africa/Dakar");
N_("TZ:Africa/Dar_es_Salaam");
N_("TZ:Africa/Djibouti");
N_("TZ:Africa/Douala");
N_("TZ:Africa/El_Aaiun");
N_("TZ:Africa/Freetown");
N_("TZ:Africa/Gaborone");
N_("TZ:Africa/Harare");
N_("TZ:Africa/Johannesburg");
N_("TZ:Africa/Juba");
N_("TZ:Africa/Kampala");
N_("TZ:Africa/Khartoum");
N_("TZ:Africa/Kigali");
N_("TZ:Africa/Kinshasa");
N_("TZ:Africa/Lagos");
N_("TZ:Africa/Libreville");
N_("TZ:Africa/Lome");
N_("TZ:Africa/Luanda");
N_("TZ:Africa/Lubumbashi");
N_("TZ:Africa/Lusaka");
N_("TZ:Africa/Malabo");
N_("TZ:Africa/Maputo");
N_("TZ:Africa/Maseru");
N_("TZ:Africa/Mbabane");
N_("TZ:Africa/Mogadishu");
N_("TZ:Africa/Monrovia");
N_("TZ:Africa/Nairobi");
N_("TZ:Africa/Ndjamena");
N_("TZ:Africa/Niamey");
N_("TZ:Africa/Nouakchott");
N_("TZ:Africa/Ouagadougou");
N_("TZ:Africa/Porto-Novo");
N_("TZ:Africa/Pretoria");
N_("TZ:Africa/Sao_Tome");
N_("TZ:Africa/Timbuktu");
N_("TZ:Africa/Tripoli");
N_("TZ:Africa/Tunis");
N_("TZ:Africa/Windhoek");
N_("TZ:America/Adak");
N_("TZ:America/Anchorage");
N_("TZ:America/Anguilla");
N_("TZ:America/Antigua");
N_("TZ:America/Araguaina");
N_("TZ:America/Argentina/Buenos_Aires");
N_("TZ:America/Argentina/Catamarca");
N_("TZ:America/Argentina/ComodRivadavia");
N_("TZ:America/Argentina/Cordoba");
N_("TZ:America/Argentina/Jujuy");
N_("TZ:America/Argentina/La_Rioja");
N_("TZ:America/Argentina/Mendoza");
N_("TZ:America/Argentina/Rio_Gallegos");
N_("TZ:America/Argentina/Salta");
N_("TZ:America/Argentina/San_Juan");
N_("TZ:America/Argentina/San_Luis");
N_("TZ:America/Argentina/Tucuman");
N_("TZ:America/Argentina/Ushuaia");
N_("TZ:America/Aruba");
N_("TZ:America/Asuncion");
N_("TZ:America/Atikokan");
N_("TZ:America/Atka");
N_("TZ:America/Bahia");
N_("TZ:America/Bahia_Banderas");
N_("TZ:America/Barbados");
N_("TZ:America/Belem");
N_("TZ:America/Belize");
N_("TZ:America/Blanc-Sablon");
N_("TZ:America/Boa_Vista");
N_("TZ:America/Bogota");
N_("TZ:America/Boise");
N_("TZ:America/Buenos_Aires");
N_("TZ:America/Calgary");
N_("TZ:America/Cambridge_Bay");
N_("TZ:America/Campo_Grande");
N_("TZ:America/Cancun");
N_("TZ:America/Caracas");
N_("TZ:America/Catamarca");
N_("TZ:America/Cayenne");
N_("TZ:America/Cayman");
N_("TZ:America/Chicago");
N_("TZ:America/Chihuahua");
N_("TZ:America/Coral_Harbour");
N_("TZ:America/Cordoba");
N_("TZ:America/Costa_Rica");
N_("TZ:America/Creston");
N_("TZ:America/Cuiaba");
N_("TZ:America/Curacao");
N_("TZ:America/Danmarkshavn");
N_("TZ:America/Dawson");
N_("TZ:America/Dawson_Creek");
N_("TZ:America/Denver");
N_("TZ:America/Detroit");
N_("TZ:America/Dominica");
N_("TZ:America/Edmonton");
N_("TZ:America/Eirunepe");
N_("TZ:America/El_Salvador");
N_("TZ:America/Ensenada");
N_("TZ:America/Fort_Nelson");
N_("TZ:America/Fort_Wayne");
N_("TZ:America/Fortaleza");
N_("TZ:America/Fredericton");
N_("TZ:America/Glace_Bay");
N_("TZ:America/Godthab");
N_("TZ:America/Goose_Bay");
N_("TZ:America/Grand_Turk");
N_("TZ:America/Grenada");
N_("TZ:America/Guadeloupe");
N_("TZ:America/Guatemala");
N_("TZ:America/Guayaquil");
N_("TZ:America/Guyana");
N_("TZ:America/Halifax");
N_("TZ:America/Havana");
N_("TZ:America/Hermosillo");
N_("TZ:America/Indiana/Indianapolis");
N_("TZ:America/Indiana/Knox");
N_("TZ:America/Indiana/Marengo");
N_("TZ:America/Indiana/Petersburg");
N_("TZ:America/Indiana/Tell_City");
N_("TZ:America/Indiana/Vevay");
N_("TZ:America/Indiana/Vincennes");
N_("TZ:America/Indiana/Winamac");
N_("TZ:America/Indianapolis");
N_("TZ:America/Inuvik");
N_("TZ:America/Iqaluit");
N_("TZ:America/Jamaica");
N_("TZ:America/Jujuy");
N_("TZ:America/Juneau");
N_("TZ:America/Kentucky/Louisville");
N_("TZ:America/Kentucky/Monticello");
N_("TZ:America/Knox_IN");
N_("TZ:America/Kralendijk");
N_("TZ:America/La_Paz");
N_("TZ:America/Lima");
N_("TZ:America/Los_Angeles");
N_("TZ:America/Louisville");
N_("TZ:America/Lower_Princes");
N_("TZ:America/Maceio");
N_("TZ:America/Managua");
N_("TZ:America/Manaus");
N_("TZ:America/Marigot");
N_("TZ:America/Martinique");
N_("TZ:America/Matamoros");
N_("TZ:America/Mazatlan");
N_("TZ:America/Mendoza");
N_("TZ:America/Menominee");
N_("TZ:America/Merida");
N_("TZ:America/Metlakatla");
N_("TZ:America/Mexico_City");
N_("TZ:America/Miquelon");
N_("TZ:America/Moncton");
N_("TZ:America/Monterrey");
N_("TZ:America/Montevideo");
N_("TZ:America/Montreal");
N_("TZ:America/Montserrat");
N_("TZ:America/Nassau");
N_("TZ:America/New_York");
N_("TZ:America/Nipigon");
N_("TZ:America/Nome");
N_("TZ:America/Noronha");
N_("TZ:America/North_Dakota/Beulah");
N_("TZ:America/North_Dakota/Center");
N_("TZ:America/North_Dakota/New_Salem");
N_("TZ:America/Nuuk");
N_("TZ:America/Ojinaga");
N_("TZ:America/Ontario");
N_("TZ:America/Panama");
N_("TZ:America/Pangnirtung");
N_("TZ:America/Paramaribo");
N_("TZ:America/Phoenix");
N_("TZ:America/Port-au-Prince");
N_("TZ:America/Port_of_Spain");
N_("TZ:America/Porto_Acre");
N_("TZ:America/Porto_Velho");
N_("TZ:America/Puerto_Rico");
N_("TZ:America/Punta_Arenas");
N_("TZ:America/Rainy_River");
N_("TZ:America/Rankin_Inlet");
N_("TZ:America/Recife");
N_("TZ:America/Regina");
N_("TZ:America/Resolute");
N_("TZ:America/Rio_Branco");
N_("TZ:America/Rosario");
N_("TZ:America/Santa_Isabel");
N_("TZ:America/Santarem");
N_("TZ:America/Santiago");
N_("TZ:America/Santo_Domingo");
N_("TZ:America/Sao_Paulo");
N_("TZ:America/Saskatoon");
N_("TZ:America/Scoresbysund");
N_("TZ:America/Shiprock");
N_("TZ:America/Sitka");
N_("TZ:America/St_Barthelemy");
N_("TZ:America/St_Johns");
N_("TZ:America/St_Kitts");
N_("TZ:America/St_Lucia");
N_("TZ:America/St_Thomas");
N_("TZ:America/St_Vincent");
N_("TZ:America/Swift_Current");
N_("TZ:America/Tegucigalpa");
N_("TZ:America/Thule");
N_("TZ:America/Thunder_Bay");
N_("TZ:America/Tijuana");
N_("TZ:America/Toronto");
N_("TZ:America/Tortola");
N_("TZ:America/Vancouver");
N_("TZ:America/Virgin");
N_("TZ:America/Whitehorse");
N_("TZ:America/Winnipeg");
N_("TZ:America/Yakutat");
N_("TZ:America/Yellowknife");
N_("TZ:Antarctica/Casey");
N_("TZ:Antarctica/Davis");
N_("TZ:Antarctica/DumontDUrville");
N_("TZ:Antarctica/Macquarie");
N_("TZ:Antarctica/Mawson");
N_("TZ:Antarctica/McMurdo");
N_("TZ:Antarctica/Palmer");
N_("TZ:Antarctica/Rothera");
N_("TZ:Antarctica/South_Pole");
N_("TZ:Antarctica/Syowa");
N_("TZ:Antarctica/Troll");
N_("TZ:Antarctica/Vostok");
N_("TZ:Arctic/Longyearbyen");
N_("TZ:Asia/Aden");
N_("TZ:Asia/Almaty");
N_("TZ:Asia/Amman");
N_("TZ:Asia/Anadyr");
N_("TZ:Asia/Aqtau");
N_("TZ:Asia/Aqtobe");
N_("TZ:Asia/Ashgabat");
N_("TZ:Asia/Ashkhabad");
N_("TZ:Asia/Atyrau");
N_("TZ:Asia/Baghdad");
N_("TZ:Asia/Bahrain");
N_("TZ:Asia/Baku");
N_("TZ:Asia/Bangkok");
N_("TZ:Asia/Barnaul");
N_("TZ:Asia/Beijing");
N_("TZ:Asia/Beirut");
N_("TZ:Asia/Bishkek");
N_("TZ:Asia/Brunei");
N_("TZ:Asia/Calcutta");
N_("TZ:Asia/Chita");
N_("TZ:Asia/Choibalsan");
N_("TZ:Asia/Chongqing");
N_("TZ:Asia/Chungking");
N_("TZ:Asia/Colombo");
N_("TZ:Asia/Dacca");
N_("TZ:Asia/Damascus");
N_("TZ:Asia/Dhaka");
N_("TZ:Asia/Dili");
N_("TZ:Asia/Dubai");
N_("TZ:Asia/Dushanbe");
N_("TZ:Asia/Famagusta");
N_("TZ:Asia/Gaza");
N_("TZ:Asia/Harbin");
N_("TZ:Asia/Hebron");
N_("TZ:Asia/Ho_Chi_Minh");
N_("TZ:Asia/Hong_Kong");
N_("TZ:Asia/Hovd");
N_("TZ:Asia/Irkutsk");
N_("TZ:Asia/Istanbul");
N_("TZ:Asia/Jakarta");
N_("TZ:Asia/Jayapura");
N_("TZ:Asia/Jerusalem");
N_("TZ:Asia/Kabul");
N_("TZ:Asia/Kamchatka");
N_("TZ:Asia/Karachi");
N_("TZ:Asia/Kashgar");
N_("TZ:Asia/Kathmandu");
N_("TZ:Asia/Katmandu");
N_("TZ:Asia/Khandyga");
N_("TZ:Asia/Kolkata");
N_("TZ:Asia/Krasnoyarsk");
N_("TZ:Asia/Kuala_Lumpur");
N_("TZ:Asia/Kuching");
N_("TZ:Asia/Kuwait");
N_("TZ:Asia/Macao");
N_("TZ:Asia/Macau");
N_("TZ:Asia/Magadan");
N_("TZ:Asia/Makassar");
N_("TZ:Asia/Manila");
N_("TZ:Asia/Muscat");
N_("TZ:Asia/Nicosia");
N_("TZ:Asia/Novokuznetsk");
N_("TZ:Asia/Novosibirsk");
N_("TZ:Asia/Omsk");
N_("TZ:Asia/Oral");
N_("TZ:Asia/Phnom_Penh");
N_("TZ:Asia/Pontianak");
N_("TZ:Asia/Pyongyang");
N_("TZ:Asia/Qatar");
N_("TZ:Asia/Qostanay");
N_("TZ:Asia/Qyzylorda");
N_("TZ:Asia/Rangoon");
N_("TZ:Asia/Riyadh");
N_("TZ:Asia/Saigon");
N_("TZ:Asia/Sakhalin");
N_("TZ:Asia/Samarkand");
N_("TZ:Asia/Seoul");
N_("TZ:Asia/Shanghai");
N_("TZ:Asia/Singapore");
N_("TZ:Asia/Srednekolymsk");
N_("TZ:Asia/Taipei");
N_("TZ:Asia/Tashkent");
N_("TZ:Asia/Tbilisi");
N_("TZ:Asia/Tehran");
N_("TZ:Asia/Tel_Aviv");
N_("TZ:Asia/Thimbu");
N_("TZ:Asia/Thimphu");
N_("TZ:Asia/Tokyo");
N_("TZ:Asia/Tomsk");
N_("TZ:Asia/Ujung_Pandang");
N_("TZ:Asia/Ulaanbaatar");
N_("TZ:Asia/Ulan_Bator");
N_("TZ:Asia/Urumqi");
N_("TZ:Asia/Ust-Nera");
N_("TZ:Asia/Vientiane");
N_("TZ:Asia/Vladivostok");
N_("TZ:Asia/Yakutsk");
N_("TZ:Asia/Yangon");
N_("TZ:Asia/Yekaterinburg");
N_("TZ:Asia/Yerevan");
N_("TZ:Atlantic/Azores");
N_("TZ:Atlantic/Bermuda");
N_("TZ:Atlantic/Canary");
N_("TZ:Atlantic/Cape_Verde");
N_("TZ:Atlantic/Faeroe");
N_("TZ:Atlantic/Faroe");
N_("TZ:Atlantic/Jan_Mayen");
N_("TZ:Atlantic/Madeira");
N_("TZ:Atlantic/Reykjavik");
N_("TZ:Atlantic/South_Georgia");
N_("TZ:Atlantic/St_Helena");
N_("TZ:Atlantic/Stanley");
N_("TZ:Australia/ACT");
N_("TZ:Australia/Adelaide");
N_("TZ:Australia/Brisbane");
N_("TZ:Australia/Broken_Hill");
N_("TZ:Australia/Canberra");
N_("TZ:Australia/Currie");
N_("TZ:Australia/Darwin");
N_("TZ:Australia/Eucla");
N_("TZ:Australia/Hobart");
N_("TZ:Australia/LHI");
N_("TZ:Australia/Lindeman");
N_("TZ:Australia/Lord_Howe");
N_("TZ:Australia/Melbourne");
N_("TZ:Australia/NSW");
N_("TZ:Australia/North");
N_("TZ:Australia/Perth");
N_("TZ:Australia/Queensland");
N_("TZ:Australia/South");
N_("TZ:Australia/Sydney");
N_("TZ:Australia/Tasmania");
N_("TZ:Australia/Victoria");
N_("TZ:Australia/West");
N_("TZ:Australia/Yancowinna");
N_("TZ:Brazil/Acre");
N_("TZ:Brazil/DeNoronha");
N_("TZ:Brazil/East");
N_("TZ:Brazil/West");
N_("TZ:CET");
N_("TZ:CST6CDT");
N_("TZ:Canada/Atlantic");
N_("TZ:Canada/Central");
N_("TZ:Canada/Eastern");
N_("TZ:Canada/Mountain");
N_("TZ:Canada/Newfoundland");
N_("TZ:Canada/Pacific");
N_("TZ:Canada/Saskatchewan");
N_("TZ:Canada/Yukon");
N_("TZ:Chile/Continental");
N_("TZ:Chile/EasterIsland");
N_("TZ:Cuba");
N_("TZ:EET");
N_("TZ:EST");
N_("TZ:EST5EDT");
N_("TZ:Egypt");
N_("TZ:Eire");
N_("TZ:Etc/GMT");
N_("TZ:Etc/GMT+0");
N_("TZ:Etc/GMT+1");
N_("TZ:Etc/GMT+10");
N_("TZ:Etc/GMT+11");
N_("TZ:Etc/GMT+12");
N_("TZ:Etc/GMT+2");
N_("TZ:Etc/GMT+3");
N_("TZ:Etc/GMT+4");
N_("TZ:Etc/GMT+5");
N_("TZ:Etc/GMT+6");
N_("TZ:Etc/GMT+7");
N_("TZ:Etc/GMT+8");
N_("TZ:Etc/GMT+9");
N_("TZ:Etc/GMT-0");
N_("TZ:Etc/GMT-1");
N_("TZ:Etc/GMT-10");
N_("TZ:Etc/GMT-11");
N_("TZ:Etc/GMT-12");
N_("TZ:Etc/GMT-13");
N_("TZ:Etc/GMT-14");
N_("TZ:Etc/GMT-2");
N_("TZ:Etc/GMT-3");
N_("TZ:Etc/GMT-4");
N_("TZ:Etc/GMT-5");
N_("TZ:Etc/GMT-6");
N_("TZ:Etc/GMT-7");
N_("TZ:Etc/GMT-8");
N_("TZ:Etc/GMT-9");
N_("TZ:Etc/GMT0");
N_("TZ:Etc/Greenwich");
N_("TZ:Etc/UCT");
N_("TZ:Etc/UTC");
N_("TZ:Etc/Universal");
N_("TZ:Etc/Zulu");
N_("TZ:Europe/Amsterdam");
N_("TZ:Europe/Andorra");
N_("TZ:Europe/Astrakhan");
N_("TZ:Europe/Athens");
N_("TZ:Europe/Belfast");
N_("TZ:Europe/Belgrade");
N_("TZ:Europe/Berlin");
N_("TZ:Europe/Bratislava");
N_("TZ:Europe/Brussels");
N_("TZ:Europe/Bucharest");
N_("TZ:Europe/Budapest");
N_("TZ:Europe/Busingen");
N_("TZ:Europe/Chisinau");
N_("TZ:Europe/Copenhagen");
N_("TZ:Europe/Dublin");
N_("TZ:Europe/Gibraltar");
N_("TZ:Europe/Guernsey");
N_("TZ:Europe/Helsinki");
N_("TZ:Europe/Isle_of_Man");
N_("TZ:Europe/Istanbul");
N_("TZ:Europe/Jersey");
N_("TZ:Europe/Kaliningrad");
N_("TZ:Europe/Kiev");
N_("TZ:Europe/Kirov");
N_("TZ:Europe/Lisbon");
N_("TZ:Europe/Ljubljana");
N_("TZ:Europe/London");
N_("TZ:Europe/Luxembourg");
N_("TZ:Europe/Madrid");
N_("TZ:Europe/Malta");
N_("TZ:Europe/Mariehamn");
N_("TZ:Europe/Minsk");
N_("TZ:Europe/Monaco");
N_("TZ:Europe/Moscow");
N_("TZ:Europe/Nicosia");
N_("TZ:Europe/Oslo");
N_("TZ:Europe/Paris");
N_("TZ:Europe/Podgorica");
N_("TZ:Europe/Prague");
N_("TZ:Europe/Riga");
N_("TZ:Europe/Rome");
N_("TZ:Europe/Samara");
N_("TZ:Europe/San_Marino");
N_("TZ:Europe/Sarajevo");
N_("TZ:Europe/Saratov");
N_("TZ:Europe/Simferopol");
N_("TZ:Europe/Skopje");
N_("TZ:Europe/Sofia");
N_("TZ:Europe/Stockholm");
N_("TZ:Europe/Tallinn");
N_("TZ:Europe/Tirane");
N_("TZ:Europe/Tiraspol");
N_("TZ:Europe/Ulyanovsk");
N_("TZ:Europe/Uzhgorod");
N_("TZ:Europe/Vaduz");
N_("TZ:Europe/Vatican");
N_("TZ:Europe/Vienna");
N_("TZ:Europe/Vilnius");
N_("TZ:Europe/Volgograd");
N_("TZ:Europe/Warsaw");
N_("TZ:Europe/Zagreb");
N_("TZ:Europe/Zaporozhye");
N_("TZ:Europe/Zurich");
N_("TZ:Factory");
N_("TZ:GB");
N_("TZ:GB-Eire");
N_("TZ:GMT");
N_("TZ:GMT+0");
N_("TZ:GMT-0");
N_("TZ:GMT0");
N_("TZ:Greenwich");
N_("TZ:HST");
N_("TZ:Hongkong");
N_("TZ:Iceland");
N_("TZ:Indian/Antananarivo");
N_("TZ:Indian/Chagos");
N_("TZ:Indian/Christmas");
N_("TZ:Indian/Cocos");
N_("TZ:Indian/Comoro");
N_("TZ:Indian/Kerguelen");
N_("TZ:Indian/Mahe");
N_("TZ:Indian/Maldives");
N_("TZ:Indian/Mauritius");
N_("TZ:Indian/Mayotte");
N_("TZ:Indian/Reunion");
N_("TZ:Iran");
N_("TZ:Israel");
N_("TZ:Jamaica");
N_("TZ:Japan");
N_("TZ:Kwajalein");
N_("TZ:Libya");
N_("TZ:MET");
N_("TZ:MST");
N_("TZ:MST7MDT");
N_("TZ:Mexico/BajaNorte");
N_("TZ:Mexico/BajaSur");
N_("TZ:Mexico/General");
N_("TZ:NZ");
N_("TZ:NZ-CHAT");
N_("TZ:Navajo");
N_("TZ:PST8PDT");
N_("TZ:Pacific/Apia");
N_("TZ:Pacific/Auckland");
N_("TZ:Pacific/Bougainville");
N_("TZ:Pacific/Chatham");
N_("TZ:Pacific/Chuuk");
N_("TZ:Pacific/Easter");
N_("TZ:Pacific/Efate");
N_("TZ:Pacific/Enderbury");
N_("TZ:Pacific/Fakaofo");
N_("TZ:Pacific/Fiji");
N_("TZ:Pacific/Funafuti");
N_("TZ:Pacific/Galapagos");
N_("TZ:Pacific/Gambier");
N_("TZ:Pacific/Guadalcanal");
N_("TZ:Pacific/Guam");
N_("TZ:Pacific/Honolulu");
N_("TZ:Pacific/Johnston");
N_("TZ:Pacific/Kiritimati");
N_("TZ:Pacific/Kosrae");
N_("TZ:Pacific/Kwajalein");
N_("TZ:Pacific/Majuro");
N_("TZ:Pacific/Marquesas");
N_("TZ:Pacific/Midway");
N_("TZ:Pacific/Nauru");
N_("TZ:Pacific/Niue");
N_("TZ:Pacific/Norfolk");
N_("TZ:Pacific/Noumea");
N_("TZ:Pacific/Pago_Pago");
N_("TZ:Pacific/Palau");
N_("TZ:Pacific/Pitcairn");
N_("TZ:Pacific/Pohnpei");
N_("TZ:Pacific/Ponape");
N_("TZ:Pacific/Port_Moresby");
N_("TZ:Pacific/Rarotonga");
N_("TZ:Pacific/Saipan");
N_("TZ:Pacific/Samoa");
N_("TZ:Pacific/Tahiti");
N_("TZ:Pacific/Tarawa");
N_("TZ:Pacific/Kanton");
N_("TZ:Pacific/Tongatapu");
N_("TZ:Pacific/Truk");
N_("TZ:Pacific/Wake");
N_("TZ:Pacific/Wallis");
N_("TZ:Pacific/Yap");
N_("TZ:Poland");
N_("TZ:Portugal");
N_("TZ:ROK");
N_("TZ:Singapore");
N_("TZ:Turkey");
N_("TZ:UCT");
N_("TZ:US/Alaska");
N_("TZ:US/Aleutian");
N_("TZ:US/Arizona");
N_("TZ:US/Central");
N_("TZ:US/East-Indiana");
N_("TZ:US/Eastern");
N_("TZ:US/Hawaii");
N_("TZ:US/Indiana-Starke");
N_("TZ:US/Michigan");
N_("TZ:US/Mountain");
N_("TZ:US/Pacific");
N_("TZ:US/Samoa");
N_("TZ:UTC");
N_("TZ:Universal");
N_("TZ:W-SU");
N_("TZ:WET");
N_("TZ:Zulu");

1;
