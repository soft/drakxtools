package partition_table::gpt; # $Id: gpt.pm 274112 2012-05-24 21:27:32Z peroyvind $





@ISA = qw(partition_table::raw);

use common;
use partition_table::raw;
use c;

my $nb_primary = 128;

sub read_one {
    my ($hd, $_sector) = @_;

    c::get_disk_type($hd->{file}) eq "gpt" or die "$hd->{device} not a GPT disk ($hd->{file})";

    my @pt;
    foreach (c::get_disk_partitions($hd->{file})) {
	log::l($_);
	if (/^([^ ]*) ([^ ]*) ([^ ]*) (.*) \((\d*),(\d*),(\d*)\)$/) {
            my %p;
            $p{part_number} = $1;
            $p{real_device} = $2;
            $p{fs_type} = $3;
            $p{pt_type} = 0xba;
            $p{start} = $5;
            $p{size} = $7;
            if (c::partition_get_flag($hd->{file}, $p{part_number}, 'PED_PARTITION_BOOT') == 1) {
                $p{pt_type} = 0xef;
            }
            elsif (c::partition_get_flag($hd->{file}, $p{part_number}, 'PED_PARTITION_BIOS_GRUB') == 1) {
                $p{pt_type} = 0xef02;
            }
            @pt[$p{part_number}-1] = \%p;
        }
    }

    for (my $part_number = 1; $part_number < $nb_primary; $part_number++) {
	next if exists($pt[$part_number-1]);
	$pt[$part_number-1] = { part_number => $part_number };
    }

    \@pt;
}

sub write {
    my ($hd, $_sector, $_pt, $_info) = @_;

    # Initialize the disk if initialization command was invoked by user (either for changing
    # partition table type, of for clearing all partitions)
    my $init_idx = find_first_index { $_->[0] eq 'init' } $hd->{will_tell_kernel};
    if (defined($init_idx)) {
        c::set_disk_type($hd->{file}, "gpt");
        # Remove all will_tell_kernel's up to this 'init' action, since disk initialization takes care
        # of deleting existing partitions
        splice(@{$hd->{will_tell_kernel}}, 0, $init_idx + 1);
    }

    foreach (@{$hd->{will_tell_kernel}}) {
        my ($action, $part_number, $o_start, $o_size) = @$_;
        print "($action, $part_number, $o_start, $o_size)\n";
        my $partuuid;
        my $add = ($action eq 'add');
        my $del = ($action eq 'del');
        if (member($action, 'add', 'del', 'resize')) {
            $_ = [];
        }
        if ($action eq 'resize') {
            $partuuid = c::partition_gpt_get_partuuid($hd->{file}, $part_number);
            $del = 1;
            $add = 1;
        }
        if ($del) {
            c::disk_del_partition($hd->{file}, $part_number) or die "failed to del partition";
        }
        if ($add) {
            my $part = $_pt->[$part_number - 1];
            if ($ptype eq 'swap') {
                # Workaround for parted's partition type name
                $ptype = 'linux-swap';
            }
            c::disk_add_partition($hd->{file}, $o_start, $o_size, $part->{fs_type}) or die "failed to add partition";
            if ($part->{pt_type} == 0xef) {
                c::partition_set_flag($hd->{file}, $part_number, 'PED_PARTITION_BOOT', 1) or die "failed to set boot flag to the partition";
            }
            elsif ($part->{pt_type} == 0xef02) {
                c::partition_set_flag($hd->{file}, $part_number, 'PED_PARTITION_BIOS_GRUB', 1) or die "failed to set bios_grub flag to the partition";
            }
            if ($action eq 'resize') {
                c::partition_gpt_set_partuuid($hd->{file}, $part_number, $partuuid);
            }
        }
    }
    common::sync();
    1;
}

sub empty_raw { { raw => [ ({}) x $nb_primary ] } }

sub initialize {
    my ($class, $hd) = @_;
    $hd->{primary} = empty_raw();
    bless $hd, $class;
}

sub can_add { &can_raw_add }

1;
