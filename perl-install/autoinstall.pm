package autoinstall;

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Module for facilitating autoinstallation implementation.

# The autoinstallation mode is turned on if the file /etc/sysconfig/autoinstall is present.
# This file may contain param=value pairs that will override default values (the rest of parameters will remain default).

# The $enabled variable contains 1 if autoinstallation mode is activated, and 0 otherwise.
our $enabled = 0;

# The %params contains parameter=>value pairs for data that are normally user-selectable.
our %params;


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Module internals

use common;

# This array contains default values for autoinstaller.
# Possible values for each key are described in parentheses.
my %params_default = {
	# Language parameters
	'LANGUAGE_USE_GEOIP' => 1,                    # Prefer GeoIP for language, if available (1/0 for yes/no)
	'LANGUAGE' => 'ru',                           # Default language if GeoIP is unavailable of LANGUAGE_USE_GEOIP=0 (lang.pm, keys of %langs)
	'UTF8' => 1,                                  # Use UTF-8 encoding (1/0)

	# Timezone parameters
	'TIMEZONE_USE_GEOIP' => 1,                    # Prefer GeoIP for timezone, if available (1/0)
	'TIMEZONE' => 'Europe/Moscow',                # Default timezone if GeoIP is unavailable of TIMEZONE_USE_GEOIP=0 (files from /usr/share/zoneinfo/)
	'UTC' => 0,                                   # Whether computer's clock is set to UTC or local time (1/0)
	'NTP' => 0,                                   # Use NTP for synchronizing time (1/0)
	'NTP_SERVER' => 'pool.ntp.org',               # NTP server to use for synchronizing

	# Keyboard layout and hotkey for toggling
	'KEYBOARD' => 'ru',                           # Secondary keyboard layout (keyboard.pm, keys of %keyboards for non-SPARC branch)
	'KEYBOARD_TOGGLE' => 'ctrl_shift_toggle',     # Hotkey for toggling keyboard layouts (keyboard.pm, keys of %grp_toggles)

	# Partitioning
	'PARTITIONING_SCHEME' => '',                  # Partitioning scheme - currently unsupported, reserved for future
	                                              # At the moment only clean installing with erasing the whole disk is implemented.

	# Bootloader
	'GRUB_TIMEOUT' => 5,                          # Timeout (in seconds) before automatic booting the default item in Grub
	'GRUB_DISABLE_LINUX_UUID' => 0,               # Disable using UUIDs for disk names in the Grub config (1/0)

	# Authentication type
	'AUTH_TYPE' => 'local',                       # Authentication type: 'local' for local users or 'msad' for Microsoft Active Directory

	# Microsoft Active Directory parameters
	'MSAD_DOMAIN' => '',                          # Name of the domain to join
	'MSAD_OU' => '',                              # Organization Unit (empty string to use default OU)
	'MSAD_ADMIN_NAME' => 'Administrator',         # AD Administrator account name
	'MSAD_ADMIN_PASSWORD' => 'password',          # AD Administrator's password

	# Network hostname
	'HOSTNAME' => '%u-%m-%r.%d',                  # Hostname template. Replacing rules:
	                                              #   %u -> USER_NAME
	                                              #   %d -> MSAD_DOMAIN
	                                              #   %m -> auto-guessed machine name ('laptop' or 'desktop' if no vendor-specific name is detected)
	                                              #   %r -> randomly generated suffix consisting of 8 latin letters and/or digits
	                                              # Leading and trailing hyphens and dots (e.g. because of empty replacements) are automatically removed.

	# Local root
	'LOCAL_ROOT_DISABLED'  => 1,                  # Disable local root account (1/0)
	'LOCAL_ROOT_PASSWORD'  => 'rootpassword',     # Local root password (if the account is enabled)

	# Local user account
	'USER_ICON' => 'user-3',                      # Icon/avatar (name of a file from /usr/share/mdk/faces/ without extension)
	'USER_REALNAME' => 'Пользователь',            # Real name
	'USER_NAME' => 'user',                        # Account name
	'USER_PASSWORD' => '',                        # Password
	'USER_SHELL' => '/bin/bash',                  # Command-line shell
	'USER_UID' => 500,                            # Custom UID
	'USER_GID' => 500,                            # Custom GID
	'USER_GROUPS' => '',                          # Groups to add the user into

	# Services
	'SERVICE_SMB' => 'auto',                      # Samba (1/0 - enable/disable; 'auto' - leave default)
	'SERVICE_OPENVPN' => 'auto',                  # OpenVPN
	'SERVICE_SSHD' => 'auto',                     # sshd
	'SERVICE_CUPS' => 'auto'                      # CUPS
};

# Initialization
if (-e '/etc/sysconfig/autoinstall') {
	$enabled = 1;
	# Assign default values, then overwrite them with those read from the file
	%params = %params_default;
	my %params_file = getVarsFromSh('/etc/sysconfig/autoinstall');
	$params{$_} = $params_file{$_} foreach (keys(%params_file));
}

1;
